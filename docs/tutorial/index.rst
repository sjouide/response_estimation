********
Tutorial
********

In this tutorial, we'll assume that ``vre_template_tool`` is already installed on your system.
If that's not the case, see :doc:`Installation Section</install>`.

This tutorial will walk you through these tasks:

.. toctree::
   :maxdepth: 2

   tutorial

VRE template Tool is written in Python_. If you're new to the language you might want to
start to learn Python quickly, the `Python Tutorial`_ is a good resource.

.. _Python: https://www.python.org
.. _Python Tutorial: https://docs.python.org/3/tutorial/

**What next**

Now that you have and idea of what the ``vre_template_tool`` package provides,
you should investigate the parts of the package most useful for you and see an example
of how to use it.


:doc:`Reference Section</reference/index>` provides details on VRE template Tool.

:doc:`Examples Section</examples>` provides an example using VRE template Tool.